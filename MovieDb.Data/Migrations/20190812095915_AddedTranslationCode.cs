﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieDb.Data.Migrations
{
    public partial class AddedTranslationCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Code",
                table: "Locales",
                newName: "CultureCode");

            migrationBuilder.RenameIndex(
                name: "IX_Locales_Code",
                table: "Locales",
                newName: "IX_Locales_CultureCode");

            migrationBuilder.AddColumn<string>(
                name: "TranslationCode",
                table: "Locales",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Locales",
                keyColumn: "Id",
                keyValue: new Guid("11111111-1111-1111-1111-111111111111"),
                columns: new[] { "CultureCode", "TranslationCode" },
                values: new object[] { "en-US", "en" });

            migrationBuilder.UpdateData(
                table: "Locales",
                keyColumn: "Id",
                keyValue: new Guid("22222222-2222-2222-2222-222222222222"),
                column: "TranslationCode",
                value: "el");

            migrationBuilder.UpdateData(
                table: "Locales",
                keyColumn: "Id",
                keyValue: new Guid("33333333-3333-3333-3333-333333333333"),
                column: "TranslationCode",
                value: "ru");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TranslationCode",
                table: "Locales");

            migrationBuilder.RenameColumn(
                name: "CultureCode",
                table: "Locales",
                newName: "Code");

            migrationBuilder.RenameIndex(
                name: "IX_Locales_CultureCode",
                table: "Locales",
                newName: "IX_Locales_Code");

            migrationBuilder.UpdateData(
                table: "Locales",
                keyColumn: "Id",
                keyValue: new Guid("11111111-1111-1111-1111-111111111111"),
                column: "Code",
                value: "en");
        }
    }
}
