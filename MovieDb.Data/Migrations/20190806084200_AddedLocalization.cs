﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieDb.Data.Migrations
{
    public partial class AddedLocalization : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                "IX_Movies_Name",
                "Movies");

            migrationBuilder.DropIndex(
                "IX_Movies_Title",
                "Movies");

            migrationBuilder.DropIndex(
                "IX_Genres_Name",
                "Genres");

            migrationBuilder.DropIndex(
                "IX_Genres_Title",
                "Genres");

            migrationBuilder.DropIndex(
                "IX_ContributorTypes_Name",
                "ContributorTypes");

            migrationBuilder.DropIndex(
                "IX_ContributorTypes_Title",
                "ContributorTypes");

            migrationBuilder.DropIndex(
                "IX_Contributors_Name",
                "Contributors");

            migrationBuilder.DropIndex(
                "IX_Contributors_Title",
                "Contributors");

            migrationBuilder.DropColumn(
                "Description",
                "Movies");

            migrationBuilder.DropColumn(
                "Name",
                "Movies");

            migrationBuilder.DropColumn(
                "Title",
                "Movies");

            migrationBuilder.DropColumn(
                "Description",
                "Genres");

            migrationBuilder.DropColumn(
                "Name",
                "Genres");

            migrationBuilder.DropColumn(
                "Title",
                "Genres");

            migrationBuilder.DropColumn(
                "Description",
                "ContributorTypes");

            migrationBuilder.DropColumn(
                "Name",
                "ContributorTypes");

            migrationBuilder.DropColumn(
                "Title",
                "ContributorTypes");

            migrationBuilder.DropColumn(
                "Description",
                "Contributors");

            migrationBuilder.DropColumn(
                "Name",
                "Contributors");

            migrationBuilder.DropColumn(
                "Title",
                "Contributors");

            migrationBuilder.CreateTable(
                "Locales",
                table => new
                {
                    Id = table.Column<Guid>(),
                    Name = table.Column<string>(maxLength: 128),
                    Code = table.Column<string>(maxLength: 16),
                    IsDefault = table.Column<bool>()
                },
                constraints: table => { table.PrimaryKey("PK_Locales", x => x.Id); });

            migrationBuilder.CreateTable(
                "Localizations",
                table => new
                {
                    Id = table.Column<Guid>(),
                    LocaleId = table.Column<Guid>(),
                    GenreId = table.Column<Guid>(nullable: true),
                    MovieId = table.Column<Guid>(nullable: true),
                    ContributorId = table.Column<Guid>(nullable: true),
                    ContributorTypeId = table.Column<Guid>(nullable: true),
                    Title = table.Column<string>(maxLength: 128),
                    Name = table.Column<string>(maxLength: 128),
                    Description = table.Column<string>(maxLength: 2048, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Localizations", x => x.Id);
                    table.ForeignKey(
                        "FK_Localizations_Contributors_ContributorId",
                        x => x.ContributorId,
                        "Contributors",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_Localizations_ContributorTypes_ContributorTypeId",
                        x => x.ContributorTypeId,
                        "ContributorTypes",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_Localizations_Genres_GenreId",
                        x => x.GenreId,
                        "Genres",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_Localizations_Locales_LocaleId",
                        x => x.LocaleId,
                        "Locales",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_Localizations_Movies_MovieId",
                        x => x.MovieId,
                        "Movies",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                "Locales",
                new[] {"Id", "Code", "IsDefault", "Name"},
                new object[] {new Guid("11111111-1111-1111-1111-111111111111"), "en-US", true, "English"});

            migrationBuilder.InsertData(
                "Locales",
                new[] {"Id", "Code", "IsDefault", "Name"},
                new object[] {new Guid("22222222-2222-2222-2222-222222222222"), "el", false, "Greek"});

            migrationBuilder.InsertData(
                "Locales",
                new[] {"Id", "Code", "IsDefault", "Name"},
                new object[] {new Guid("33333333-3333-3333-3333-333333333333"), "ru-RU", false, "Russian"});

            migrationBuilder.CreateIndex(
                "IX_Localizations_ContributorId",
                "Localizations",
                "ContributorId");

            migrationBuilder.CreateIndex(
                "IX_Localizations_ContributorTypeId",
                "Localizations",
                "ContributorTypeId");

            migrationBuilder.CreateIndex(
                "IX_Localizations_GenreId",
                "Localizations",
                "GenreId");

            migrationBuilder.CreateIndex(
                "IX_Localizations_LocaleId",
                "Localizations",
                "LocaleId");

            migrationBuilder.CreateIndex(
                "IX_Localizations_MovieId",
                "Localizations",
                "MovieId");

            migrationBuilder.CreateIndex(
                "IX_Localizations_Name",
                "Localizations",
                "Name");

            migrationBuilder.CreateIndex(
                "IX_Localizations_Title",
                "Localizations",
                "Title");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "Localizations");

            migrationBuilder.DropTable(
                "Locales");

            migrationBuilder.AddColumn<string>(
                "Description",
                "Movies",
                maxLength: 2048,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                "Name",
                "Movies",
                maxLength: 128,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                "Title",
                "Movies",
                maxLength: 128,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                "Description",
                "Genres",
                maxLength: 2048,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                "Name",
                "Genres",
                maxLength: 128,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                "Title",
                "Genres",
                maxLength: 128,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                "Description",
                "ContributorTypes",
                maxLength: 2048,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                "Name",
                "ContributorTypes",
                maxLength: 128,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                "Title",
                "ContributorTypes",
                maxLength: 128,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                "Description",
                "Contributors",
                maxLength: 2048,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                "Name",
                "Contributors",
                maxLength: 128,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                "Title",
                "Contributors",
                maxLength: 128,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                "IX_Movies_Name",
                "Movies",
                "Name");

            migrationBuilder.CreateIndex(
                "IX_Movies_Title",
                "Movies",
                "Title");

            migrationBuilder.CreateIndex(
                "IX_Genres_Name",
                "Genres",
                "Name");

            migrationBuilder.CreateIndex(
                "IX_Genres_Title",
                "Genres",
                "Title");

            migrationBuilder.CreateIndex(
                "IX_ContributorTypes_Name",
                "ContributorTypes",
                "Name");

            migrationBuilder.CreateIndex(
                "IX_ContributorTypes_Title",
                "ContributorTypes",
                "Title");

            migrationBuilder.CreateIndex(
                "IX_Contributors_Name",
                "Contributors",
                "Name");

            migrationBuilder.CreateIndex(
                "IX_Contributors_Title",
                "Contributors",
                "Title");
        }
    }
}