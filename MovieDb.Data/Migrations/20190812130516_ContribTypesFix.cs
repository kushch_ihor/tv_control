﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieDb.Data.Migrations
{
    public partial class ContribTypesFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_MovieContributorTypes",
                table: "MovieContributorTypes");

            migrationBuilder.DropIndex(
                name: "IX_MovieContributorTypes_MovieId_ContributorId",
                table: "MovieContributorTypes");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "MovieContributorTypes");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MovieContributorTypes",
                table: "MovieContributorTypes",
                columns: new[] { "MovieId", "ContributorId", "ContributorTypeId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_MovieContributorTypes",
                table: "MovieContributorTypes");

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "MovieContributorTypes",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_MovieContributorTypes",
                table: "MovieContributorTypes",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_MovieContributorTypes_MovieId_ContributorId",
                table: "MovieContributorTypes",
                columns: new[] { "MovieId", "ContributorId" });
        }
    }
}
