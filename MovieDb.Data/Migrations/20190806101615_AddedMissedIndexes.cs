﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieDb.Data.Migrations
{
    public partial class AddedMissedIndexes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                "IX_Locales_Code",
                "Locales",
                "Code");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                "IX_Locales_Code",
                "Locales");
        }
    }
}