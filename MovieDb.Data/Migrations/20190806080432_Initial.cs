﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieDb.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "Contributors",
                table => new
                {
                    Id = table.Column<Guid>(),
                    Title = table.Column<string>(maxLength: 128),
                    Name = table.Column<string>(maxLength: 128),
                    Description = table.Column<string>(maxLength: 2048, nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Contributors", x => x.Id); });

            migrationBuilder.CreateTable(
                "ContributorTypes",
                table => new
                {
                    Id = table.Column<Guid>(),
                    Title = table.Column<string>(maxLength: 128),
                    Name = table.Column<string>(maxLength: 128),
                    Description = table.Column<string>(maxLength: 2048, nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_ContributorTypes", x => x.Id); });

            migrationBuilder.CreateTable(
                "Genres",
                table => new
                {
                    Id = table.Column<Guid>(),
                    Title = table.Column<string>(maxLength: 128),
                    Name = table.Column<string>(maxLength: 128),
                    Description = table.Column<string>(maxLength: 2048, nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Genres", x => x.Id); });

            migrationBuilder.CreateTable(
                "Movies",
                table => new
                {
                    Id = table.Column<Guid>(),
                    Title = table.Column<string>(maxLength: 128),
                    Name = table.Column<string>(maxLength: 128),
                    Description = table.Column<string>(maxLength: 2048, nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Movies", x => x.Id); });

            migrationBuilder.CreateTable(
                "MovieContributors",
                table => new
                {
                    MovieId = table.Column<Guid>(),
                    ContributorId = table.Column<Guid>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieContributors", x => new {x.MovieId, x.ContributorId});
                    table.ForeignKey(
                        "FK_MovieContributors_Contributors_ContributorId",
                        x => x.ContributorId,
                        "Contributors",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_MovieContributors_Movies_MovieId",
                        x => x.MovieId,
                        "Movies",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "MovieGenres",
                table => new
                {
                    MovieId = table.Column<Guid>(),
                    GenreId = table.Column<Guid>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieGenres", x => new {x.MovieId, x.GenreId});
                    table.ForeignKey(
                        "FK_MovieGenres_Genres_GenreId",
                        x => x.GenreId,
                        "Genres",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_MovieGenres_Movies_MovieId",
                        x => x.MovieId,
                        "Movies",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "MovieContributorTypes",
                table => new
                {
                    Id = table.Column<Guid>(),
                    MovieId = table.Column<Guid>(),
                    ContributorId = table.Column<Guid>(),
                    ContributorTypeId = table.Column<Guid>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieContributorTypes", x => x.Id);
                    table.ForeignKey(
                        "FK_MovieContributorTypes_ContributorTypes_ContributorTypeId",
                        x => x.ContributorTypeId,
                        "ContributorTypes",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_MovieContributorTypes_MovieContributors_MovieId_ContributorId",
                        x => new {x.MovieId, x.ContributorId},
                        "MovieContributors",
                        new[] {"MovieId", "ContributorId"},
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                "IX_Contributors_Name",
                "Contributors",
                "Name");

            migrationBuilder.CreateIndex(
                "IX_Contributors_Title",
                "Contributors",
                "Title");

            migrationBuilder.CreateIndex(
                "IX_ContributorTypes_Name",
                "ContributorTypes",
                "Name");

            migrationBuilder.CreateIndex(
                "IX_ContributorTypes_Title",
                "ContributorTypes",
                "Title");

            migrationBuilder.CreateIndex(
                "IX_Genres_Name",
                "Genres",
                "Name");

            migrationBuilder.CreateIndex(
                "IX_Genres_Title",
                "Genres",
                "Title");

            migrationBuilder.CreateIndex(
                "IX_MovieContributors_ContributorId",
                "MovieContributors",
                "ContributorId");

            migrationBuilder.CreateIndex(
                "IX_MovieContributorTypes_ContributorTypeId",
                "MovieContributorTypes",
                "ContributorTypeId");

            migrationBuilder.CreateIndex(
                "IX_MovieContributorTypes_MovieId_ContributorId",
                "MovieContributorTypes",
                new[] {"MovieId", "ContributorId"});

            migrationBuilder.CreateIndex(
                "IX_MovieGenres_GenreId",
                "MovieGenres",
                "GenreId");

            migrationBuilder.CreateIndex(
                "IX_Movies_Name",
                "Movies",
                "Name");

            migrationBuilder.CreateIndex(
                "IX_Movies_Title",
                "Movies",
                "Title");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "MovieContributorTypes");

            migrationBuilder.DropTable(
                "MovieGenres");

            migrationBuilder.DropTable(
                "ContributorTypes");

            migrationBuilder.DropTable(
                "MovieContributors");

            migrationBuilder.DropTable(
                "Genres");

            migrationBuilder.DropTable(
                "Contributors");

            migrationBuilder.DropTable(
                "Movies");
        }
    }
}