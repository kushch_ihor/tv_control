﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieDb.Data.Migrations
{
    public partial class AddedCascade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Localizations_Contributors_ContributorId",
                table: "Localizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Localizations_ContributorTypes_ContributorTypeId",
                table: "Localizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Localizations_Genres_GenreId",
                table: "Localizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Localizations_Movies_MovieId",
                table: "Localizations");

            migrationBuilder.AddForeignKey(
                name: "FK_Localizations_Contributors_ContributorId",
                table: "Localizations",
                column: "ContributorId",
                principalTable: "Contributors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Localizations_ContributorTypes_ContributorTypeId",
                table: "Localizations",
                column: "ContributorTypeId",
                principalTable: "ContributorTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Localizations_Genres_GenreId",
                table: "Localizations",
                column: "GenreId",
                principalTable: "Genres",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Localizations_Movies_MovieId",
                table: "Localizations",
                column: "MovieId",
                principalTable: "Movies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Localizations_Contributors_ContributorId",
                table: "Localizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Localizations_ContributorTypes_ContributorTypeId",
                table: "Localizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Localizations_Genres_GenreId",
                table: "Localizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Localizations_Movies_MovieId",
                table: "Localizations");

            migrationBuilder.AddForeignKey(
                name: "FK_Localizations_Contributors_ContributorId",
                table: "Localizations",
                column: "ContributorId",
                principalTable: "Contributors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Localizations_ContributorTypes_ContributorTypeId",
                table: "Localizations",
                column: "ContributorTypeId",
                principalTable: "ContributorTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Localizations_Genres_GenreId",
                table: "Localizations",
                column: "GenreId",
                principalTable: "Genres",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Localizations_Movies_MovieId",
                table: "Localizations",
                column: "MovieId",
                principalTable: "Movies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
