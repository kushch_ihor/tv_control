﻿using System;
using Microsoft.EntityFrameworkCore;
using MovieDb.Data.Entities;

namespace MovieDb.Data
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext()
        {
        }

        public MovieDbContext(DbContextOptions<MovieDbContext> options)
            : base(options)
        {
        }

        public DbSet<Genre> Genres { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Contributor> Contributors { get; set; }
        public DbSet<ContributorType> ContributorTypes { get; set; }
        public DbSet<MovieGenre> MovieGenre { get; set; }
        public DbSet<MovieContributor> MovieContributors { get; set; }
        public DbSet<MovieContributorType> MovieContributorTypes { get; set; }
        public DbSet<Locale> Locales { get; set; }
        public DbSet<Localization> Localizations { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");
            base.OnModelCreating(builder);

            builder.Entity<Locale>(entity =>
            {
                entity.ToTable("Locales");
                entity.HasKey(x => new {x.Id});
                entity.HasIndex(x => x.CultureCode);
                entity.Property(x => x.Name)
                    .HasMaxLength(128)
                    .IsRequired();
                entity.Property(x => x.CultureCode)
                    .HasMaxLength(16)
                    .IsRequired();
                entity.HasData(
                    new
                    {
                        Id = new Guid(Guid.Empty.ToString().Replace('0', '1')),
                        Name = "English",
                        CultureCode = "en-US",
                        TranslationCode = "en",
                        IsDefault = true
                    },
                    new
                    {
                        Id = new Guid(Guid.Empty.ToString().Replace('0', '2')),
                        Name = "Greek",
                        CultureCode = "el",
                        TranslationCode = "el",
                        IsDefault = false
                    },
                    new
                    {
                        Id = new Guid(Guid.Empty.ToString().Replace('0', '3')),
                        Name = "Russian",
                        CultureCode = "ru-RU",
                        TranslationCode = "ru",
                        IsDefault = false
                    });
            });


            builder.Entity<Localization>(entity =>
            {
                entity.ToTable("Localizations");
                entity.HasKey(x => x.Id);
                entity.HasIndex(x => x.Title);
                entity.HasIndex(x => x.Name);
                entity.Property(x => x.Title)
                    .HasMaxLength(128)
                    .IsRequired();
                entity.Property(x => x.Name)
                    .HasMaxLength(128)
                    .IsRequired();
                entity.Property(x => x.Description)
                    .HasMaxLength(2048)
                    .IsRequired(false);
                entity.HasOne(x => x.Locale)
                    .WithMany(x => x.Localizations)
                    .HasForeignKey(x => x.LocaleId);
                entity.HasOne(x => x.Genre)
                    .WithMany(x => x.Localizations)
                    .HasForeignKey(x => x.GenreId)
                    .IsRequired(false)
                    .OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(x => x.Movie)
                    .WithMany(x => x.Localizations)
                    .HasForeignKey(x => x.MovieId)
                    .IsRequired(false)
                    .OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(x => x.Contributor)
                    .WithMany(x => x.Localizations)
                    .HasForeignKey(x => x.ContributorId)
                    .IsRequired(false)
                    .OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(x => x.ContributorType)
                    .WithMany(x => x.Localizations)
                    .HasForeignKey(x => x.ContributorTypeId)
                    .IsRequired(false)
                    .OnDelete(DeleteBehavior.Cascade);
            });


            builder.Entity<Genre>(entity =>
            {
                entity.ToTable("Genres");
                entity.HasKey(x => x.Id);
            });

            builder.Entity<Movie>(entity =>
            {
                entity.ToTable("Movies");
                entity.HasKey(x => x.Id);
            });

            builder.Entity<Contributor>(entity =>
            {
                entity.ToTable("Contributors");
                entity.HasKey(x => x.Id);
            });


            builder.Entity<ContributorType>(entity =>
            {
                entity.ToTable("ContributorTypes");
                entity.HasKey(x => x.Id);
            });

            builder.Entity<MovieGenre>(entity =>
            {
                entity.ToTable("MovieGenres");
                entity.HasKey(x => new {x.MovieId, x.GenreId});
                entity.HasOne(x => x.Movie)
                    .WithMany(x => x.MovieGenres)
                    .HasForeignKey(x => x.MovieId);
                entity.HasOne(x => x.Genre)
                    .WithMany(x => x.MovieGenres)
                    .HasForeignKey(x => x.GenreId);
            });

            builder.Entity<MovieContributor>(entity =>
            {
                entity.ToTable("MovieContributors");
                entity.HasKey(x => new {x.MovieId, x.ContributorId});
                entity.HasOne(x => x.Movie)
                    .WithMany(x => x.MovieContributors)
                    .HasForeignKey(x => x.MovieId);
                entity.HasOne(x => x.Contributor)
                    .WithMany(x => x.MovieContributors)
                    .HasForeignKey(x => x.ContributorId);
            });

            builder.Entity<MovieContributorType>(entity =>
            {
                entity.ToTable("MovieContributorTypes");
                entity.HasKey(x => new {x.MovieId, x.ContributorId, x.ContributorTypeId});
                entity.HasOne(x => x.MovieContributor)
                    .WithMany(x => x.MovieContributorTypes)
                    .HasForeignKey(x => new {x.MovieId, x.ContributorId});
                entity.HasOne(x => x.ContributorType)
                    .WithMany(x => x.MovieContributorTypes)
                    .HasForeignKey(x => x.ContributorTypeId);
            });
        }
    }
}