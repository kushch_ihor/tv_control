﻿using System;

namespace MovieDb.Data.Entities
{
    public class MovieContributorType
    {
        public Guid MovieId { get; set; }
        public Guid ContributorId { get; set; }
        public MovieContributor MovieContributor { get; set; }
        public Guid ContributorTypeId { get; set; }
        public ContributorType ContributorType { get; set; }
    }
}