﻿using System;
using System.Collections.Generic;

namespace MovieDb.Data.Entities
{
    public class Contributor
    {
        public Guid Id { get; set; }
        public ICollection<Localization> Localizations { get; set; }
        public ICollection<MovieContributor> MovieContributors { get; set; }
    }
}