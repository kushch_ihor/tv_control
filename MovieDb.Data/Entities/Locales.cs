﻿using System;
using System.Collections.Generic;

namespace MovieDb.Data.Entities
{
    public class Locale
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string CultureCode { get; set; }
        public string TranslationCode { get; set; }
        public bool IsDefault { get; set; }
        public ICollection<Localization> Localizations { get; set; }
    }
}