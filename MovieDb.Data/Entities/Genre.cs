﻿using System;
using System.Collections.Generic;

namespace MovieDb.Data.Entities
{
    public class Genre
    {
        public Guid Id { get; set; }
        public ICollection<Localization> Localizations { get; set; }
        public ICollection<MovieGenre> MovieGenres { get; set; }
    }
}