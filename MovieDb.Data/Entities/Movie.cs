﻿using System;
using System.Collections.Generic;

namespace MovieDb.Data.Entities
{
    public class Movie
    {
        public Guid Id { get; set; }
        public ICollection<Localization> Localizations { get; set; }
        public ICollection<MovieGenre> MovieGenres { get; set; }
        public ICollection<MovieContributor> MovieContributors { get; set; }
    }
}