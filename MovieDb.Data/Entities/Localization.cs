﻿using System;

namespace MovieDb.Data.Entities
{
    public class Localization
    {
        public Guid Id { get; set; }

        public Guid LocaleId { get; set; }
        public Locale Locale { get; set; }

        public Guid? GenreId { get; set; }
        public Genre Genre { get; set; }

        public Guid? MovieId { get; set; }
        public Movie Movie { get; set; }

        public Guid? ContributorId { get; set; }
        public Contributor Contributor { get; set; }

        public Guid? ContributorTypeId { get; set; }
        public ContributorType ContributorType { get; set; }

        public string Title { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}