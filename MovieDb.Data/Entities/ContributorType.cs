﻿using System;
using System.Collections.Generic;

namespace MovieDb.Data.Entities
{
    public class ContributorType
    {
        public Guid Id { get; set; }
        public ICollection<Localization> Localizations { get; set; }
        public ICollection<MovieContributorType> MovieContributorTypes { get; set; }
    }
}