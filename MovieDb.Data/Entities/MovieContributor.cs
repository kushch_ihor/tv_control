﻿using System;
using System.Collections.Generic;

namespace MovieDb.Data.Entities
{
    public class MovieContributor
    {
        public Guid MovieId { get; set; }
        public Movie Movie { get; set; }
        public Guid ContributorId { get; set; }
        public Contributor Contributor { get; set; }
        public ICollection<MovieContributorType> MovieContributorTypes { get; set; }
    }
}