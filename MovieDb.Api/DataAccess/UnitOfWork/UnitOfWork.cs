﻿using System.Threading;
using System.Threading.Tasks;
using MovieDb.Api.Interfaces;
using MovieDb.Api.Repositories;
using MovieDb.Api.Repositories.Interfaces;
using MovieDb.Data;

namespace MovieDb.Api
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MovieDbContext _context;
        private IGenresRepository _genres;
        private ILocalesRepository _locales;
        private ILocalizationsRepository _localizations;
        private IContributorTypesRepository _contributorTypes;
        private IContributorsRepository _contributors;
        private IMoviesRepository _movies;
        private IMovieContributorsRepository _movieContributors;
        private IMovieContributorTypesRepository _movieContributorTypes;
        private IMovieGenresRepository _movieGenres;

        public UnitOfWork(MovieDbContext context)
        {
            _context = context;
        }

        public IGenresRepository Genres => _genres ?? (_genres = new GenresRepository(_context));
        public ILocalesRepository Locales => _locales ?? (_locales = new LocalesRepository(_context));
        public ILocalizationsRepository Localizations =>
            _localizations ?? (_localizations = new LocalizationsRepository(_context));
        public IContributorTypesRepository ContributorTypes =>
            _contributorTypes ?? (_contributorTypes = new ContributorTypesRepository(_context));
        public IContributorsRepository Contributors =>
            _contributors ?? (_contributors = new ContributorsRepository(_context));
        public IMoviesRepository Movies =>
            _movies ?? (_movies = new MoviesRepository(_context));
        public IMovieGenresRepository MovieGenres =>
            _movieGenres ?? (_movieGenres = new MovieGenresRepository(_context));
        public IMovieContributorsRepository MovieContributors =>
            _movieContributors ?? (_movieContributors = new MovieContributorsRepository(_context));
        public IMovieContributorTypesRepository MovieContributorTypes =>
            _movieContributorTypes ?? (_movieContributorTypes = new MovieContributorTypesRepository(_context));

        public void SubmitChanges()
        {
            _context.SaveChanges();
        }

        public async Task SubmitChangesAsync(CancellationToken cancellationToken)
        {
            await _context.SaveChangesAsync(true, cancellationToken);
        }
    }
}