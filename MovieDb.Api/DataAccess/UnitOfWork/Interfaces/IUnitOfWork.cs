﻿using System.Threading;
using System.Threading.Tasks;
using MovieDb.Api.Repositories.Interfaces;

namespace MovieDb.Api.Interfaces
{
    public interface IUnitOfWork
    {
        IGenresRepository Genres { get; }
        IContributorTypesRepository ContributorTypes { get; }
        IContributorsRepository Contributors { get; }
        ILocalesRepository Locales { get; }
        ILocalizationsRepository Localizations { get; }
        IMoviesRepository Movies { get; }
        IMovieContributorsRepository MovieContributors { get; }
        IMovieContributorTypesRepository MovieContributorTypes { get; }
        IMovieGenresRepository MovieGenres { get; }
        void SubmitChanges();
        Task SubmitChangesAsync(CancellationToken cancellationToken = default);
    }
}