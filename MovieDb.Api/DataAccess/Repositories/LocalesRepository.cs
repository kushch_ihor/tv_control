﻿using MovieDb.Api.Repositories.Abstract;
using MovieDb.Api.Repositories.Interfaces;
using MovieDb.Data;
using MovieDb.Data.Entities;

namespace MovieDb.Api.Repositories
{
    public class LocalesRepository : BaseRepository<Locale>, ILocalesRepository
    {
        public LocalesRepository(MovieDbContext context) : base(context)
        {
        }
    }
}