﻿using MovieDb.Api.Repositories.Abstract.Interfaces;
using MovieDb.Data.Entities;

namespace MovieDb.Api.Repositories.Interfaces
{
    public interface IContributorTypesRepository : IBaseRepository<ContributorType>
    {
    }
}