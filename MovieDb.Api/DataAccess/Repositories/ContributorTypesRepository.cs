﻿using MovieDb.Api.Repositories.Abstract;
using MovieDb.Api.Repositories.Interfaces;
using MovieDb.Data;
using MovieDb.Data.Entities;

namespace MovieDb.Api.Repositories
{
    public class ContributorTypesRepository : BaseRepository<ContributorType>, IContributorTypesRepository
    {
        public ContributorTypesRepository(MovieDbContext context) : base(context)
        {
        }
    }
}