﻿using MovieDb.Api.Repositories.Abstract;
using MovieDb.Api.Repositories.Interfaces;
using MovieDb.Data;
using MovieDb.Data.Entities;

namespace MovieDb.Api.Repositories
{
    public class MovieContributorsRepository : BaseRepository<MovieContributor>, IMovieContributorsRepository
    {
        public MovieContributorsRepository(MovieDbContext context) : base(context)
        {
        }
    }
}