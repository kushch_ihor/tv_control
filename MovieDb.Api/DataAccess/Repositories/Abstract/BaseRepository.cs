﻿using System.Linq;
using System.Threading.Tasks;
using MovieDb.Api.Repositories.Abstract.Interfaces;
using MovieDb.Data;

namespace MovieDb.Api.Repositories.Abstract
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private readonly MovieDbContext _context;

        protected BaseRepository(MovieDbContext context)
        {
            _context = context;
        }

        public void AttachRange(params TEntity[] entities)
        {
            _context.AttachRange(entities);
        }

        public void Attach(TEntity entity)
        {
            _context.Attach(entity);
        }

        public void Delete(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
        }

        public void DeleteRange(IQueryable<TEntity> entities)
        {
            _context.Set<TEntity>().RemoveRange(entities);
        }

        public TEntity Get(object id)
        {
            return _context.Set<TEntity>().Find(id);
        }

        public IQueryable<TEntity> GetAll()
        {
            return _context.Set<TEntity>();
        }

        public void Insert(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
        }

        public void Update(TEntity entity)
        {
            _context.Set<TEntity>().Update(entity);
        }

        public void SubmitChanges()
        {
            _context.SaveChanges();
        }

        public Task SubmitChangesAsync()
        {
            return _context.SaveChangesAsync(true);
        }

        public void BeginTransaction()
        {
            _context.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            _context.Database.CommitTransaction();
        }

        public void RollbackTransaction()
        {
            _context.Database.RollbackTransaction();
        }
    }
}