﻿using System.Linq;
using System.Threading.Tasks;

namespace MovieDb.Api.Repositories.Abstract.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        TEntity Get(object id);
        IQueryable<TEntity> GetAll();

        void Attach(TEntity entity);
        void AttachRange(params TEntity[] entities);

        void Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void DeleteRange(IQueryable<TEntity> entities);
        void BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();

        void SubmitChanges();
        Task SubmitChangesAsync();
    }
}