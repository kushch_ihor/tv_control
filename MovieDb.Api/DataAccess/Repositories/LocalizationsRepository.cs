﻿using MovieDb.Api.Repositories.Abstract;
using MovieDb.Api.Repositories.Interfaces;
using MovieDb.Data;
using MovieDb.Data.Entities;

namespace MovieDb.Api.Repositories
{
    public class LocalizationsRepository : BaseRepository<Localization>, ILocalizationsRepository
    {
        public LocalizationsRepository(MovieDbContext context) : base(context)
        {
        }
    }
}