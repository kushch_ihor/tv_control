﻿using MovieDb.Api.Repositories.Abstract;
using MovieDb.Api.Repositories.Interfaces;
using MovieDb.Data;
using MovieDb.Data.Entities;

namespace MovieDb.Api.Repositories
{
    public class MoviesRepository : BaseRepository<Movie>, IMoviesRepository
    {
        public MoviesRepository(MovieDbContext context) : base(context)
        {
        }
    }
}