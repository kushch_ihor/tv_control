﻿using MovieDb.Api.Repositories.Abstract;
using MovieDb.Api.Repositories.Interfaces;
using MovieDb.Data;
using MovieDb.Data.Entities;

namespace MovieDb.Api.Repositories
{
    public class MovieGenresRepository : BaseRepository<MovieGenre>, IMovieGenresRepository
    {
        public MovieGenresRepository(MovieDbContext context) : base(context)
        {
        }
    }
}