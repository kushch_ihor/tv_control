﻿using MovieDb.Api.Repositories.Abstract;
using MovieDb.Api.Repositories.Interfaces;
using MovieDb.Data;
using MovieDb.Data.Entities;

namespace MovieDb.Api.Repositories
{
    public class ContributorsRepository : BaseRepository<Contributor>, IContributorsRepository
    {
        public ContributorsRepository(MovieDbContext context) : base(context)
        {
        }
    }
}