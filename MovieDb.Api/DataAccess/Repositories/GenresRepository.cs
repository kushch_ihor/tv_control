﻿using MovieDb.Api.Repositories.Abstract;
using MovieDb.Api.Repositories.Interfaces;
using MovieDb.Data;
using MovieDb.Data.Entities;

namespace MovieDb.Api.Repositories
{
    public class GenresRepository : BaseRepository<Genre>, IGenresRepository
    {
        public GenresRepository(MovieDbContext context) : base(context)
        {
        }
    }
}