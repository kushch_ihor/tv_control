﻿using MovieDb.Api.Repositories.Abstract;
using MovieDb.Api.Repositories.Interfaces;
using MovieDb.Data;
using MovieDb.Data.Entities;

namespace MovieDb.Api.Repositories
{
    public class MovieContributorTypesRepository : BaseRepository<MovieContributorType>, IMovieContributorTypesRepository
    {
        public MovieContributorTypesRepository(MovieDbContext context) : base(context)
        {
        }
    }
}