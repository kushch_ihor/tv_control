﻿namespace MovieDb.Api.Services.Settings
{
    public class CloudTranslateSettings
    {
        public string ApiKey { get; set; }
    }
}