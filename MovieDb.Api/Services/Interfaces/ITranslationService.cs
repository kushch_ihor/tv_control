﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MovieDb.Api.Dto;

namespace MovieDb.Api.Services.Interfaces
{
    public interface ITranslationService
    {
        Task<NewItemDto> GetTranslatedAsync(NewItemDto source, string targetLangCode);
    }
}
