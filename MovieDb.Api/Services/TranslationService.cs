﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Logging;
using Google.Cloud.Translation.V2;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MovieDb.Api.Dto;
using MovieDb.Api.Services.Interfaces;
using MovieDb.Api.Services.Settings;

namespace MovieDb.Api.Services
{
    public class TranslationService : ITranslationService
    {
        private TranslationClient _client;
        private ILogger<TranslationService> _logger;
        private readonly CloudTranslateSettings _settings;
        protected TranslationClient Client => _client ?? (_client = TranslationClient.CreateFromApiKey(_settings.ApiKey));

        public TranslationService(ILogger<TranslationService> logger, IOptions<CloudTranslateSettings> options)
        {
            _logger = logger;
            _settings = options.Value;
        }

        public async Task<NewItemDto> GetTranslatedAsync(NewItemDto source, string targetLangCode)
        {
            var sourceList = new List<string> {source.Name, source.Title, source.Description};
            var translation = await Client.TranslateTextAsync(sourceList, targetLangCode);
            var result = new ItemDto()
            {
                Name = translation[0].TranslatedText,
                Title = translation[1].TranslatedText,
                Description = translation[2].TranslatedText
            };
            return result;
        }
    }
}
