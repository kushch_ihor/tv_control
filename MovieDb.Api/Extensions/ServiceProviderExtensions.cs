﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace MovieDb.Api.Extensions
{
    public static class ServiceProvideExtensions
    {
        public static IServiceScope GetScopedService<T>(this IServiceProvider serviceProvider, out T service)
        {
            var scope = serviceProvider.CreateScope();
            service = scope.ServiceProvider.GetRequiredService<T>();
            return scope;
        }
    }
}