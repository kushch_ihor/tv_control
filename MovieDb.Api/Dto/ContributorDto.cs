﻿using System.Collections.Generic;

namespace MovieDb.Api.Dto
{
    public class ContributorDto: ItemDto
    {
        public ICollection<ItemDto> ContributorTypes { get; set; }
    }
}