﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieDb.Api.Dto
{
    public class NewContributorDto
    {
        [Required] public Guid Id { get; set; }
        public ICollection<Guid> ContributorTypeIds { get; set; }
    }
}