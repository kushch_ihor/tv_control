﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDb.Api.Dto
{
    public class NewMovieDto : NewItemDto
    {
        public ICollection<Guid> GenreIds { get; set; }
        public ICollection<NewContributorDto> Contributors { get; set; }
    }
}
