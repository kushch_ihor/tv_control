﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using MovieDb.Data.Entities;

namespace MovieDb.Api.Dto
{
    public class MovieDto: ItemDto
    {
        public ICollection<ItemDto> Genres { get; set; }
        public ICollection<ContributorDto> Contributors { get; set; }
    }
}
