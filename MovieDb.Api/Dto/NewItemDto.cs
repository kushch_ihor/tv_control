﻿using System.ComponentModel.DataAnnotations;

namespace MovieDb.Api.Dto
{
    public class NewItemDto
    {
        [Required] [StringLength(128)] public string Name { get; set; }

        [Required] [StringLength(128)] public string Title { get; set; }

        [StringLength(2048)] public string Description { get; set; }
    }
}