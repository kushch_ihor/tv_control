﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MovieDb.Api.Dto
{
    public class ItemDto : NewItemDto
    {
        [Required] public Guid Id { get; set; }
    }
}