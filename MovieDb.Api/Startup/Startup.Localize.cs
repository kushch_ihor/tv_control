﻿using System.Data;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using MovieDb.Api.Extensions;
using MovieDb.Data;

namespace MovieDb.Api
{
    public static partial class StartupExtension
    {
        public static IApplicationBuilder UseLocalization(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetScopedService(out MovieDbContext context))
            {
                var supportedCultures = context.Locales
                    .OrderByDescending(x => x.IsDefault)
                    .Select(x => new CultureInfo(x.CultureCode))
                    .ToList();

                if (!supportedCultures.Any())
                    throw new DataException("No initial data in Locales table");

                app.UseRequestLocalization(new RequestLocalizationOptions
                {
                    DefaultRequestCulture = new RequestCulture(supportedCultures.First()),
                    SupportedCultures = supportedCultures,
                    SupportedUICultures = supportedCultures
                });
            }

            return app;
        }
    }
}