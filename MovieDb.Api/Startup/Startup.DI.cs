﻿using Microsoft.Extensions.DependencyInjection;
using MovieDb.Api.Interfaces;
using MovieDb.Api.Services;
using MovieDb.Api.Services.Interfaces;

namespace MovieDb.Api
{
    public static partial class StartupExtension
    {
        public static IServiceCollection AddDependencyInjectionRules(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<ITranslationService, TranslationService>();
            return services;
        }
    }
}