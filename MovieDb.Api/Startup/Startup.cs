﻿using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MovieDb.Api.Services.Settings;
using MovieDb.Data;

namespace MovieDb.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var environment = services
                .BuildServiceProvider()
                .GetRequiredService<IHostingEnvironment>();

            services.AddLoggingConfiguration(Configuration, environment);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddDbContext<MovieDbContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("DatabaseConnection")));

            services.AddDependencyInjectionRules();

            services.AddRouting(options => options.LowercaseUrls = true);

            services.AddODataConfiguration();

            services.AddSwaggerConfiguration();

            services.AddCors();

            services.AddOptions()
                .Configure<CloudTranslateSettings>(Configuration.GetSection("GoogleCloud"));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();

            app.UseHttpsRedirection();

            app.UseCors(builder => builder
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowAnyOrigin()
                .AllowCredentials()
                .WithExposedHeaders("Location"));

            app.UseMvc(routeBuilder =>
            {
                routeBuilder.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
                routeBuilder.EnableDependencyInjection();
                routeBuilder
                    .Expand()
                    .Select()
                    .OrderBy()
                    .Filter()
                    .MaxTop(1000);
            });
            
            app.UseLocalization();

            app.ApplyMigrations();

            app.UseSwaggerWithUi();
        }
    }
}