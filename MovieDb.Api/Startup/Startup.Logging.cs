﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace MovieDb.Api
{
    public static partial class StartupExtension
    {
        public static IServiceCollection AddLoggingConfiguration(this IServiceCollection services,
            IConfiguration configuration, IHostingEnvironment environment)
        {
            services.AddLogging(builder =>
            {
                var section = configuration.GetSection("Logging");

                builder.AddConfiguration(section)
                    .AddFilter("Microsoft.EntityFrameworkCore.Database.Command", LogLevel.Warning)
                    .AddFilter("Microsoft.EntityFrameworkCore.Infrastructure", LogLevel.Warning)
                    .AddFilter("Microsoft.AspNetCore", LogLevel.Warning);

                if (environment.IsDevelopment()) builder.AddConsole();
            });

            return services;
        }
    }
}