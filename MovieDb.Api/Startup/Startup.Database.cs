﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using MovieDb.Api.Extensions;
using MovieDb.Data;

namespace MovieDb.Api
{
    public static partial class StartupExtension
    {
        public static IApplicationBuilder ApplyMigrations(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetScopedService(out MovieDbContext context))
            {
                context.Database.Migrate();
            }

            return app;
        }
    }
}