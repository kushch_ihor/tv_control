﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MovieDb.Api.Controllers.Abstract;
using MovieDb.Api.Dto;
using MovieDb.Api.Interfaces;
using MovieDb.Api.Services.Interfaces;
using MovieDb.Data.Entities;

namespace MovieDb.Api.Controllers
{
    public class ContributorsController : BaseApiController
    {
        public ContributorsController(ILogger<ContributorsController> logger, 
            IUnitOfWork unitOfWork, 
            ITranslationService translationService) :
            base(logger, unitOfWork, translationService)
        {
        }

        // GET: api/<controller>
        [HttpGet]
        [ProducesResponseType(typeof(IList<ItemDto>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAsync()
        {
            var result = await UnitOfWork.Localizations.GetAll()
                .Where(x => x.LocaleId == LocaleId && x.ContributorId != null)
                .Select(x => new ItemDto
                {
                    Id = x.ContributorId.Value,
                    Name = x.Name,
                    Title = x.Title,
                    Description = x.Description
                })
                .ToListAsync();

            return Ok(result);
        }

        // GET api/<controller>/5
        [HttpGet("{id}", Name = "GetContributorItem")]
        [ProducesResponseType(typeof(ItemDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            var result = await UnitOfWork.Localizations.GetAll()
                .Where(x => x.LocaleId == LocaleId && x.ContributorId == id)
                .Select(x => new ItemDto
                {
                    Id = x.ContributorId.Value,
                    Name = x.Name,
                    Title = x.Title,
                    Description = x.Description
                })
                .FirstOrDefaultAsync();

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        // POST api/<controller>
        [HttpPost]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> PostAsync([FromBody] NewItemDto value)
        {
            CheckModelState();

            var item = new Contributor
            {
                Id = Guid.NewGuid()
            };
            UnitOfWork.Contributors.Insert(item);

            foreach (var locale in UnitOfWork.Locales.GetAll().ToList())
            {
                var data = value;

                if (locale.Id != LocaleId)
                    data = await TranslationService.GetTranslatedAsync(value, locale.TranslationCode);

                var localization = new Localization
                {
                    Id = Guid.NewGuid(),
                    ContributorId = item.Id,
                    LocaleId = locale.Id,
                    Title = data.Title,
                    Name = data.Name,
                    Description = data.Description
                };
                UnitOfWork.Localizations.Insert(localization);
            };

            await UnitOfWork.SubmitChangesAsync();

            return CreatedAtRoute("GetContributorItem", new {id = item.Id}, null);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> PutAsync(Guid id, [FromBody] NewItemDto value)
        {
            CheckModelState();

            var item = await UnitOfWork.Contributors.GetAll()
                .FirstOrDefaultAsync(x => x.Id == id);

            if (item == null)
                return NotFound();

            var localization = await UnitOfWork.Localizations.GetAll()
                .Where(x => x.LocaleId == LocaleId && x.ContributorId == id)
                .FirstOrDefaultAsync();

            if (localization == null)
            {
                localization = new Localization
                {
                    Id = Guid.NewGuid(),
                    ContributorId = item.Id,
                    LocaleId = LocaleId
                };
                UnitOfWork.Localizations.Insert(localization);
            }

            localization.Title = value.Title;
            localization.Name = value.Name;
            localization.Description = value.Description;

            await UnitOfWork.SubmitChangesAsync();

            return CreatedAtRoute("GetContributorItem", new { id = item.Id }, null);
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var item = await UnitOfWork.Contributors.GetAll()
                .FirstOrDefaultAsync(x => x.Id == id);

            if (item == null)
                return NotFound();

            UnitOfWork.Contributors.Delete(item);
            await UnitOfWork.SubmitChangesAsync();
            return Ok();
        }
    }
}