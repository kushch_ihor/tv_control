﻿using System;
using System.Linq;
using System.Threading;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MovieDb.Api.Interfaces;
using MovieDb.Api.Services.Interfaces;

namespace MovieDb.Api.Controllers.Abstract
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public abstract class BaseApiController : Controller
    {
        private Guid? _localeId;

        protected BaseApiController(ILogger logger,
            IUnitOfWork unitOfWork,
            ITranslationService translationService)
        {
            Logger = logger;
            UnitOfWork = unitOfWork;
            TranslationService = translationService;
        }

        protected ILogger Logger { get; }
        protected IUnitOfWork UnitOfWork { get; }
        protected ITranslationService TranslationService { get; }

        public Guid LocaleId
        {
            get
            {
                if (_localeId == null)
                {
                    var code = Thread.CurrentThread.CurrentCulture.Name;
                    _localeId = UnitOfWork.Locales.GetAll()
                        .Where(x => x.CultureCode == code)
                        .Select(x => x.Id)
                        .First();
                }

                return _localeId.Value;
            }
        }

        protected void CheckModelState()
        {
            if (!ModelState.IsValid)
            {
                var message = string.Join(" \r\n ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                var exception = new Exception(message);
                throw exception;
            }
        }
    }
}