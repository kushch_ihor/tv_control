﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MovieDb.Api.Controllers.Abstract;
using MovieDb.Api.Dto;
using MovieDb.Api.Interfaces;
using MovieDb.Api.Services.Interfaces;
using MovieDb.Data.Entities;

namespace MovieDb.Api.Controllers
{
    public partial class MoviesController : BaseApiController
    {
        public MoviesController(ILogger<MoviesController> logger,
            IUnitOfWork unitOfWork,
            ITranslationService translationService) :
            base(logger, unitOfWork, translationService)
        {
        }


        // GET api/<controller>
        [HttpGet()]
        [ProducesResponseType(typeof(IList<ItemDto>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> GetMoviesAsync()
        {
            // Could be used AutoMapper, but it takes time for test task, doing manually
            var result =  await UnitOfWork.Localizations.GetAll()
                .Where(x => x.LocaleId == LocaleId && x.MovieId != null)
                .Select(x => new MovieDto
                {
                    Id = x.MovieId.Value,
                    Name = x.Name,
                    Title = x.Title,
                    Description = x.Description
                }).ToListAsync();

            return Ok(result);
        }

        // GET api/<controller>/5
        [HttpGet("{id}", Name = "GetMovieItem")]
        [ProducesResponseType(typeof(MovieDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> GetMoviesAsync(Guid id)
        {
            var result = await UnitOfWork.Localizations.GetAll()
                .Where(x => x.LocaleId == LocaleId && x.MovieId == id)
                .Select(x => new MovieDto
                {
                    Id = x.MovieId.Value,
                    Name = x.Name,
                    Title = x.Title,
                    Description = x.Description,
                })
                .FirstOrDefaultAsync();

            if (result == null)
                return NotFound();

            result.Genres = await UnitOfWork.Localizations.GetAll()
                .Where(x => x.LocaleId == LocaleId && x.Genre.MovieGenres.Any(g=>g.MovieId == id))
                .Select(x => new ItemDto
                {
                    Id = x.GenreId.Value,
                    Name = x.Name,
                    Title = x.Title,
                    Description = x.Description,
                }).ToListAsync();

            result.Contributors = await UnitOfWork.Localizations.GetAll()
                .Where(x => x.LocaleId == LocaleId && x.Contributor.MovieContributors.Any(g => g.MovieId == id))
                .Select(x => new ContributorDto
                {
                    Id = x.ContributorId.Value,
                    Name = x.Name,
                    Title = x.Title,
                    Description = x.Description,
                }).ToListAsync();


            foreach (var item in result.Contributors)
            {
                item.ContributorTypes = await UnitOfWork.Localizations.GetAll()
                    .Where(x => x.LocaleId == LocaleId && x.ContributorType.MovieContributorTypes.Any(ct=>ct.ContributorId == item.Id && ct.MovieId == id))
                    .Select(x => new ItemDto
                    {
                        Id = x.ContributorTypeId.Value,
                        Name = x.Name,
                        Title = x.Title,
                        Description = x.Description
                    }).ToListAsync();
            }

            return Ok(result);
        }

        // POST api/<controller>
        [HttpPost()]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> PostMoviesAsync([FromBody] NewMovieDto value)
        {
            CheckModelState();

            var item = new Movie
            {
                Id = Guid.NewGuid()
            };
            UnitOfWork.Movies.Insert(item);

            foreach (var locale in UnitOfWork.Locales.GetAll().ToList())
            {
                NewItemDto data = value;

                if (locale.Id != LocaleId)
                    data = await TranslationService.GetTranslatedAsync(value, locale.TranslationCode);

                var localization = new Localization
                {
                    Id = Guid.NewGuid(),
                    MovieId = item.Id,
                    LocaleId = locale.Id,
                    Title = data.Title,
                    Name = data.Name,
                    Description = data.Description
                };
                UnitOfWork.Localizations.Insert(localization);
            };

            foreach (var genreId in value.GenreIds)
            {
                var movieGenre = new MovieGenre
                {
                    GenreId = genreId,
                    MovieId = item.Id};
                UnitOfWork.MovieGenres.Insert(movieGenre);
            }

            foreach (var contributor in value.Contributors)
            {
                var movieContributor = new MovieContributor
                {
                    ContributorId = contributor.Id,
                    MovieId = item.Id
                };
                UnitOfWork.MovieContributors.Insert(movieContributor);

                foreach (var contributorTypeId in contributor.ContributorTypeIds)
                {
                    var contributorType = new MovieContributorType
                    {
                        ContributorId = contributor.Id,
                        MovieId = item.Id,
                        ContributorTypeId = contributorTypeId
                    };
                    UnitOfWork.MovieContributorTypes.Insert(contributorType);
                }
            }

            await UnitOfWork.SubmitChangesAsync();

            return CreatedAtRoute("GetMovieItem", new { id = item.Id }, null);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> PutMoviesAsync(Guid id, [FromBody] NewMovieDto value)
        {
            CheckModelState();

            var item = await UnitOfWork.Movies.GetAll()
                .FirstOrDefaultAsync(x => x.Id == id);

            if (item == null)
                return NotFound();

            var localization = await UnitOfWork.Localizations.GetAll()
                .Where(x => x.LocaleId == LocaleId && x.MovieId == id)
                .FirstOrDefaultAsync();

            if (localization == null)
            {
                localization = new Localization
                {
                    Id = Guid.NewGuid(),
                    MovieId = item.Id,
                    LocaleId = LocaleId
                };
                UnitOfWork.Localizations.Insert(localization);
            }

            localization.Title = value.Title;
            localization.Name = value.Name;
            localization.Description = value.Description;

            //clean old value
            UnitOfWork.MovieGenres.DeleteRange(UnitOfWork.MovieGenres.GetAll().Where(c => c.MovieId == id));
            UnitOfWork.MovieContributorTypes.DeleteRange(UnitOfWork.MovieContributorTypes.GetAll().Where(c => c.MovieId == id));
            UnitOfWork.MovieContributors.DeleteRange(UnitOfWork.MovieContributors.GetAll().Where(c => c.MovieId == id));

            // set new
            foreach (var genreId in value.GenreIds)
            {
                var movieGenre = new MovieGenre
                {
                    GenreId = genreId,
                    MovieId = item.Id
                };
                UnitOfWork.MovieGenres.Insert(movieGenre);
            }

            foreach (var contributor in value.Contributors)
            {
                var movieContributor = new MovieContributor
                {
                    ContributorId = contributor.Id,
                    MovieId = item.Id
                };
                UnitOfWork.MovieContributors.Insert(movieContributor);

                foreach (var contributorTypeId in contributor.ContributorTypeIds)
                {
                    var contributorType = new MovieContributorType
                    {
                        ContributorId = contributor.Id,
                        MovieId = item.Id,
                        ContributorTypeId = contributorTypeId
                    };
                    UnitOfWork.MovieContributorTypes.Insert(contributorType);
                }
            }

            await UnitOfWork.SubmitChangesAsync();

            return CreatedAtRoute("GetMovieItem", new { id = item.Id }, null);
        }


        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var item = await UnitOfWork.Movies.GetAll()
                .FirstOrDefaultAsync(x => x.Id == id);

            if (item == null)
                return NotFound();

            UnitOfWork.Movies.Delete(item);
            await UnitOfWork.SubmitChangesAsync();
            return Ok();
        }
    }
}