import { ViroundComponent } from './components/viround/viround.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GenresComponent } from './components/genres/genres.component';
import { ContributorsTypesComponent } from './components/contributors-types/contributors-types.component';
import { ContributorsComponent } from './components/contributors/contributors.component';
import { MoviesComponent } from './components/movies/movies.component';


const routes: Routes = [
  { path: 'movies', component: MoviesComponent },
  { path: 'genres', component: GenresComponent },
  { path: 'contributors', component: ContributorsComponent },
  { path: 'contributorstypes', component: ContributorsTypesComponent },
  { path: 'viround', component: ViroundComponent },
  { path: '', redirectTo: '/movies', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
