import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'uppercaseUnderlineLess'
})
export class UppercaseUnderlineLessPipe implements PipeTransform {

  transform(value: string): string {
    if (!value) {
      return null;
    }
    return value.replace(/_/g, ' ').toUpperCase();
  }
}
