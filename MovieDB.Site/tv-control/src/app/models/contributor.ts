import { ContributorType } from './contributorType';
import { BaseModel } from './baseModel';
export class Contributor extends BaseModel {
    contributorTypes: ContributorType[]
}
