export class User {
    id: string;
    userName: string;
    eventsCount: number;
    homeCity: string;
}
