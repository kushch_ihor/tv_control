export class Category {
    constructor(obj: any) {
        this.name = obj.name;
        this.queue = obj.category_id;
        this.parentCategory = obj.parentCategory;
        this.order = this.getOrderNumber(this.name, this.parentCategory);
    }
    name: string;
    parentCategory: string;
    queue: number;
    order: number;

    private getOrderNumber(name: string, parentName: string): number {
        let result = '';
        switch (name) {
            case ('Politics'): return 0;
            case ('Sport'): result = parentName;
                            break;
            case ('Gossip'): return 20;
            case ('Others'): return 30;
            case ('Parties'): return 40;
            case ('Concerts_and_Shows'): return 50;
            case ('Exhibitions'): return 70;
            case ('Meetings'): return 75;
            case ('Online meetings'): return 80;
            case ('Adoption'): return 90;
            case ('lost_and_found_pets'): return 100;
            case ('Animal_Abuse'): return 110;
            case ('Stolen'): return 120;
            case ('Vehicles'): return 130;
            case ('Violence'): return 140;
            case ('Other_Criminal'): return 150;
            case ('Lost'): return 160;
            case ('Found'): return 170;
            case ('Missing_person'): return 180;
            case ('bad_tenant'): return 190;
            case ('bad_employer'): return 200;
            case ('bad_owner'): return 210;
            case ('bad_renter'): return 220;
            case ('bad_other'): return 230;
            case ('Discounts'): return 240;
            case ('Job_offer'): return 250;
            case ('Look_for_job'): return 260;
            case ('Funny'): return 270;
            case ('Games_News'): return 280;
            case ('Games_questions'): return 290;
            default: return 999;
        }
        if (result === 'News') {
            return 10;
        } else {
            return 60;
        }

    }
}
