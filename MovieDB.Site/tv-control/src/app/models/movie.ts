import { Contributor } from 'src/app/models/contributor';
import { Genre } from './genre';
import { BaseModel } from './baseModel';
export class Movie extends BaseModel {
    constructor(name: string, title: string, description: string) {
        super();
        this.name = name;
        this.title = title;
        this.description = description;
    }
    genres: Genre[];
    contributors: Contributor[];
}
