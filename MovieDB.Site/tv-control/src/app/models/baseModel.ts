export type ID = string;
export class BaseModel {
    id: ID;
    name: string;
    title: string;
    description: string;
}
