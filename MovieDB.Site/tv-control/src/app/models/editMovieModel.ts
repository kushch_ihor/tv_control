import { ID, BaseModel } from './baseModel';

export class EditMovie extends BaseModel {
    contributors: EditContributor[] = [];
    genres: ID[] = [];
}

export class EditContributor extends BaseModel {
    constructor(contributorId: ID, contributorName: string, contributorTypes: ID[] = []) {
        super();
        this.id = contributorId;
        this.name = contributorName;
        this.contributorTypes = contributorTypes;
    }
    contributorTypes: ID[];
    active: boolean;
}
