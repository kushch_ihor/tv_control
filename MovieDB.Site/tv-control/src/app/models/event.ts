export class Event {

    constructor(obj: any) {
        this.id = obj.id;
        this.category = obj.category;
        this.parentCategory = obj.parentCategory;
        this.created_at = new Date(obj.created_at.toString());
        this.type = obj.mediaTypeIs == null
            ? 'image'
            : obj.mediaTypeIs.type;
    }
    id: string;
    category: string;
    parentCategory: string;
    created_at: Date;
    type: string;
}
