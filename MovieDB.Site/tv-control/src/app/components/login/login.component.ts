import { AccountService } from './../../services/account.service';
import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input()
  form: FormGroup;

  constructor(private accountService: AccountService) { }

  ngOnInit() {
  }

  async login() {
    const name = '+380' + this.form.controls.username.value;
    console.log(name);
    const pass = this.form.controls.password.value;
    await this.accountService.login(name, pass);
  }
}
