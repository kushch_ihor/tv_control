import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalMovieDataComponent } from './additional-movie-data.component';

describe('AdditionalMovieDataComponent', () => {
  let component: AdditionalMovieDataComponent;
  let fixture: ComponentFixture<AdditionalMovieDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdditionalMovieDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalMovieDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
