import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { GenreService } from 'src/app/services/genre.service';
import { Genre } from 'src/app/models/genre';
import { EditMovie } from 'src/app/models/editMovieModel';
import { MatSelectChange } from '@angular/material/select';

@Component({
  selector: 'app-additional-movie-data',
  templateUrl: './additional-movie-data.component.html',
  styleUrls: ['./additional-movie-data.component.css']
})
export class AdditionalMovieDataComponent implements OnInit {

  @Input()
  editMovie: EditMovie;

  defaultGenreList: Genre[];

  onGenresChange({ value: genreIdList }: MatSelectChange) {
    this.editMovie.genres = genreIdList;
  }

  constructor(
    private genreService: GenreService
  ) { }

  ngOnInit() {
    this.genreService.getAllItems().subscribe((defaultGenreList: Genre[]) => {
      this.defaultGenreList = defaultGenreList;
    }, error => console.log(error));
  }

}
