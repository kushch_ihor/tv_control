import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContributorSelectionComponent } from './contributor-selection.component';

describe('ContributorSelectionComponent', () => {
  let component: ContributorSelectionComponent;
  let fixture: ComponentFixture<ContributorSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContributorSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContributorSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
