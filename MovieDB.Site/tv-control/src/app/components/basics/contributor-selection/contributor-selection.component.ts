import { ContributorType } from './../../../models/contributorType';
import { ContributorsTypesService } from './../../../services/contributors-types.service';
import { Component, OnInit, Input } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ID } from 'src/app/models/baseModel';
import { EditContributor } from 'src/app/models/editMovieModel';

@Component({
  selector: 'app-contributor-selection',
  templateUrl: './contributor-selection.component.html',
  styleUrls: ['./contributor-selection.component.css']
})
export class ContributorSelectionComponent implements OnInit {

  @Input()
  editContributor: EditContributor;

  typesList: ContributorType[];

  constructor(private typesService: ContributorsTypesService) { }

  onContributorChecked({ checked }: MatCheckboxChange) {
    this.editContributor.active = checked;
    if (!checked) {
      this.editContributor.contributorTypes = [];
    }
  }

  onTypeChecked({ checked }: MatCheckboxChange, contributorTypeId: ID) {
    if (checked && !this.editContributor.contributorTypes.includes(contributorTypeId)) {
      this.editContributor.contributorTypes.push(contributorTypeId);
    } else {
      this.editContributor.contributorTypes = this.editContributor.contributorTypes.filter(
        id => id !== contributorTypeId
      );
    }
    console.log(this.editContributor.contributorTypes);
  }

  isTypeActive(defaultTypeId: ID) {
    return this.editContributor.contributorTypes.includes(defaultTypeId);
  }

  ngOnInit() {
    this.typesService.getAllItems().subscribe(contributorTypeList => {
      this.typesList = contributorTypeList;
    });
  }

}
