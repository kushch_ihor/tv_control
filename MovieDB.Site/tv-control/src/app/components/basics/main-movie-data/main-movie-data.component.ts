import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { EditMovie } from 'src/app/models/editMovieModel';

@Component({
  selector: 'app-main-movie-data',
  templateUrl: './main-movie-data.component.html',
  styleUrls: ['./main-movie-data.component.css']
})
export class MainMovieDataComponent implements OnInit {
  @Input()
  editMovie: EditMovie;

  @Input()
  formGroup: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
