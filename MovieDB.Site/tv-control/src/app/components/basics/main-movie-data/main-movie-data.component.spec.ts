import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainMovieDataComponent } from './main-movie-data.component';

describe('MainMovieDataComponent', () => {
  let component: MainMovieDataComponent;
  let fixture: ComponentFixture<MainMovieDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainMovieDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainMovieDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
