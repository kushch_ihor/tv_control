import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  @Input()
  isAnySelected: boolean;
  @Input()
  isOneSelected: boolean;

  @Output() onDelete = new EventEmitter<void>();
  @Output() onEdit = new EventEmitter<void>();
  @Output() onAdd = new EventEmitter<void>();

  openEditDialog() {
    this.onEdit.emit();
  }
  deleteSelectedItems() {
    this.onDelete.emit();
  }
  addItem() {
    this.onAdd.emit();
  }

  constructor() { }

  ngOnInit() {
  }

}
