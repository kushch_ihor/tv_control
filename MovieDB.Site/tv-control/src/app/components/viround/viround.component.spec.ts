import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViroundComponent } from './viround.component';

describe('ViroundComponent', () => {
  let component: ViroundComponent;
  let fixture: ComponentFixture<ViroundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViroundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
