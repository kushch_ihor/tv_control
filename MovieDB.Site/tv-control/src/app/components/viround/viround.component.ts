import { Constants } from './../../../constants';
import { AccountService } from './../../services/account.service';
import { Value } from './../../models/value';
import { Category } from './../../models/category';
import { ViroundService } from './../../services/viround.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ExcellService } from 'src/app/services/excell.service';
import { Event } from './../../models/event';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-viround',
  templateUrl: './viround.component.html',
  styleUrls: ['./viround.component.css']
})
export class ViroundComponent implements OnInit {
  events: Event[] = [];
  dates: Date[] = [];
  categories: Category[] = [];
  values: Value[] = [];
  form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });
  startReportDate: Date = new Date();
  endTeportDate: Date = new Date();
  isReport = false;
  typeOfReport: Cities = 'Own';
  accountService: AccountService;

  @ViewChild('TABLE', { static: false }) TABLE: ElementRef;

  constructor(
    private viroundService: ViroundService,
    private excellService: ExcellService,
    accountService: AccountService
  ) {
    this.accountService = accountService;
  }

  async ngOnInit() {

    await this.viroundService.getCathegories().then(result => {
      result.content.forEach(element => {
        this.categories.push(new Category(element));
      });
      this.categories = this.categories.sort((a, b) => a.order - b.order);
      this.categories.push(new Category({ name: 'Total', queue: 999 }));
    });

  }

  initCellValues() {
    let totalVideosCount = 0;
    let totalImagesCount = 0;

    this.categories.forEach(category => {
      const val = new Value();
      if (category.name === 'Sport' && category.parentCategory === 'Events') {
        val.ids.push('Events Sport');
      } else {
        val.ids.push(category.name);
      }

      this.values.push(val);
      let imagesInCategory = 0;
      let videosInCategory = 0;

      if (category.name === 'Total') {
        this.dates.forEach(date => {
          const filteredByDate = this.events.filter(({ created_at: dateValue }) => dateValue.getDate() === date.getDate());
          const videoEvents = filteredByDate.filter(item => item.type === 'video');
          const imageEvents = filteredByDate.filter(item => item.type === 'image');
          val.ids.push(date.toLocaleDateString());
          val.ids.push(imageEvents.length.toString());
          val.ids.push(videoEvents.length.toString());
        });

        val.ids.push('');
        val.ids.push(totalImagesCount.toString());
        val.ids.push(totalVideosCount.toString());
      } else {

        const eventsFilteredByCategory = this.events.filter(({ category: itemCategory }) => itemCategory === category.name);

        this.dates.forEach(date => {
          let tempId = '';
          let imagesCount = 0;
          let videosCount = 0;

          eventsFilteredByCategory.forEach(event => {
            if (event.category === category.name && event.parentCategory === category.parentCategory
              && date.getFullYear() === event.created_at.getFullYear() &&
              date.getMonth() === event.created_at.getMonth() && date.getDate() === event.created_at.getDate()) {

              if (event.type === 'image') {
                tempId += event.id + ', ';
                imagesCount++;
              } else {
                tempId += `<span class='red-class'>${event.id}</span> , `;
                videosCount++;
              }
            }
          });

          val.ids.push(tempId);
          val.ids.push(imagesCount === 0 ? null : imagesCount.toString());
          val.ids.push(videosCount === 0 ? null : videosCount.toString());
          imagesInCategory += imagesCount;
          videosInCategory += videosCount;
          imagesCount = 0;
          videosCount = 0;
          tempId = '';
        });

        val.ids.push('');
        val.ids.push(imagesInCategory.toString());
        val.ids.push(videosInCategory.toString());

        totalImagesCount += imagesInCategory;
        totalVideosCount += videosInCategory;
      }
    });
  }

  async toggleIsReport() {
    this.isReport = true;
    this.values = [];
    this.events = [];
    this.dates = [];

    const monthStart = this.startReportDate;
    this.populateDates();

    if (this.typeOfReport === 'Own') {
      await this.getEventsByIdList([this.accountService.currentUser.id.toString()], monthStart);
    } else if (this.typeOfReport === 'Kyiv') {
      await this.getEventsByIdList(Constants.KuivUsers, monthStart);
    } else if (this.typeOfReport === 'Kharkiv') {
      await this.getEventsByIdList(Constants.KharkivUsers, monthStart);
    } else if (this.typeOfReport === 'Lviv') {
      await this.getEventsByIdList(Constants.LvivUsers, monthStart);
    } else if (this.typeOfReport === 'Sumy') {
      await this.getEventsByIdList(Constants.SumyUsers, monthStart);
    } else if (this.typeOfReport === 'Dnipro') {
      await this.getEventsByIdList(Constants.DniproUsers, monthStart);
    }

    this.initCellValues();
  }

  private async getEventsByIdList(ids: string[], monthStart: Date) {
    const promises = [];
    ids.forEach(id => {
      promises.push(this.viroundService.getEventsList(id));
    });
    return await Promise.all(promises).then(result =>
      result.forEach(res => {
        res.content.forEach(element => {
          const event = new Event(element);
          if (event.created_at >= monthStart) {
            this.events.push(event);
          }
        });
      }));
  }

  private populateDates() {
    let loop = new Date(this.startReportDate);
    while (loop <= this.endTeportDate) {
      this.dates.push(new Date(loop));
      const newDate = loop.setDate(loop.getDate() + 1);
      loop = new Date(newDate);
    }
  }

  async exportTable() {
    const now = new Date();
    const month = now.toLocaleString('en', { month: 'long' });
    const fileName = this.typeOfReport + ' ' + month + '.xlsx';
    this.excellService.exportAsExcelFile(this.TABLE.nativeElement, fileName);
    this.events = [];
  }

}
