import { GenreService } from './../../services/genre.service';
import { GeneralComponent } from '../general/general.component';
import { Genre } from 'src/app/models/genre';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-genres',
  templateUrl: '../general/general.component.html',
  styleUrls: ['../general/general.component.css']
})
export class GenresComponent extends GeneralComponent<Genre> {
  constructor(protected generalService: GenreService, public dialog: MatDialog) {
    super(dialog);
  }
}
