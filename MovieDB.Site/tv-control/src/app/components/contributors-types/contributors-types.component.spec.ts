import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContributorsTypesComponent } from './contributors-types.component';

describe('ContributorsTypesComponent', () => {
  let component: ContributorsTypesComponent;
  let fixture: ComponentFixture<ContributorsTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContributorsTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContributorsTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
