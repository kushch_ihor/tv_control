import { ContributorsTypesService } from './../../services/contributors-types.service';
import { ContributorType } from './../../models/contributorType';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GeneralComponent } from '../general/general.component';

@Component({
  selector: 'app-contributors-types',
  templateUrl: '../general/general.component.html',
  styleUrls: ['../general/general.component.css']
})
export class ContributorsTypesComponent extends GeneralComponent<ContributorType> {
  constructor(protected generalService: ContributorsTypesService, public dialog: MatDialog) {
    super(dialog);
  }
}
