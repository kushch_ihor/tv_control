import { ContributorsService } from './../../services/contributors.service';
import { Contributor } from './../../models/contributor';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GeneralComponent } from '../general/general.component';

@Component({
  selector: 'app-contributors',
  templateUrl: '../general/general.component.html',
  styleUrls: ['../general/general.component.css']
})
export class ContributorsComponent extends GeneralComponent<Contributor> {
  constructor(protected generalService: ContributorsService, public dialog: MatDialog) {
    super(dialog);
  }
}
