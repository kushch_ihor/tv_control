import { AddMovieComponent } from './../modals/add-movie/add-movie.component';
import { EditModalComponent } from './../modals/edit.modal';
import { MoviesService } from './../../services/movies.service';
import { Component, OnInit, ViewChild, Output } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Movie } from '../../models/movie';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { EditMovieComponent } from '../modals/edit-movie/edit-movie.component';


@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  displayedColumns: string[] = ['select', 'name', 'title'];
  dataSource = new MatTableDataSource<Movie>();
  selection = new SelectionModel<Movie>(true, []);
  movie: Movie;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(EditModalComponent, { static: true }) child: EditMovieComponent;


  constructor(private moviesService: MoviesService, public dialog: MatDialog) { }

  ngOnInit() {
    this.updateMoviesCollection();
  }

  private async updateMoviesCollection() {
    this.moviesService.getAllMovies().subscribe((result: Movie[]) => {
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
      error =>
        console.log(error)
    );
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row as Movie));
  }

  checkboxLabel(row?: Movie): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  get isAnySelected() {
    const numSelected = this.selection.selected.length;
    return numSelected !== 0;
  }
  get isOneSelected() {
    const numSelected = this.selection.selected.length;
    return numSelected === 1;
  }

  deleteSelectedItems() {
    this.selection.selected.forEach(async item => {
      await this.moviesService.deletemovie(item.id).toPromise();
      await this.updateMoviesCollection().then(() => {
        this.dataSource.data = this.dataSource.data.filter((value) => {
          return value.id !== item.id;
        });
      });
    });

    this.selection = new SelectionModel<Movie>(true, []);
  }

  async openEditDialog() {
    const edittedItem = this.selection.selected;
    const dialogRef = this.dialog.open(EditMovieComponent, {
      width: '600px',
      data: edittedItem[0]
    });

    dialogRef.afterClosed().subscribe(async () => {
      await this.updateMoviesCollection();
      this.selection = new SelectionModel<Movie>(true, []);
    });
  }

  addMovie() {
    const dialogRef = this.dialog.open(AddMovieComponent, {
      width: '600px',

    });
    dialogRef.afterClosed().subscribe(async () => {
      await this.updateMoviesCollection();
      this.selection = new SelectionModel<Movie>(true, []);
    });
  }

}
