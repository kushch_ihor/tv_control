import { EditModalComponent } from './../modals/edit.modal';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { BaseModel } from 'src/app/models/baseModel';
import { GeneralSercviceInterface } from 'src/app/services/Contracts/generalSercviceInterface';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent<T extends BaseModel> implements OnInit {

  displayedColumns: string[] = ['select', 'name', 'title'];
  dataSource = new MatTableDataSource<T>();
  selection = new SelectionModel<T>(true, []);

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  protected readonly generalService: GeneralSercviceInterface<T>;
  constructor(public dialog: MatDialog) {

  }

  ngOnInit() {
    this.updateItemsCollection();
  }

  private async updateItemsCollection() {
    this.generalService.getAllItems().subscribe((result: T[]) => {
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
      error => console.log(error)
    );
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row as T));
  }

  checkboxLabel(row?: T): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  get isAnySelected() {
    const numSelected = this.selection.selected.length;
    return numSelected !== 0;
  }

  get isOneSelected() {
    const numSelected = this.selection.selected.length;
    return numSelected === 1;
  }

  deleteSelectedItems() {
    this.selection.selected.forEach(async item => {
      await this.generalService.deleteItem(item.id).toPromise();
      await this.updateItemsCollection().then(() => {
        this.dataSource.data = this.dataSource.data.filter((value) => {
          return value.id !== item.id;
        });
      });
    });

    this.selection = new SelectionModel<T>(true, []);
  }

  openEditDialog() {
    const edittedItem = this.selection.selected;
    const dialogRef = this.dialog.open(EditModalComponent, {
      width: '600px',
      data: edittedItem[0]
    });

    dialogRef.afterClosed().subscribe(async result => {
      if (result) {
        await this.generalService.updateItem(result).toPromise().then(() => {
          this.selection = new SelectionModel<T>(true, []);
        }
        );
      }
    });
  }

  addItem() {
    const dialogRef = this.dialog.open(EditModalComponent, {
      width: '600px',
      data: { name: '', title: '', description: '' }
    });

    dialogRef.afterClosed().subscribe(async result => {
      if (result) {
        await this.generalService.createItem(result).toPromise().then(async () => {
          await this.updateItemsCollection();
          this.selection = new SelectionModel<T>(true, []);
        }
        );
      }
    });
  }

}
