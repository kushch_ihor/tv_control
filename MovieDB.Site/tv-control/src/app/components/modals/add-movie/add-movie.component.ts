import { ContributorsService } from 'src/app/services/contributors.service';
import { Component, OnInit, Inject } from '@angular/core';
import { Movie } from 'src/app/models/movie';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MoviesService } from 'src/app/services/movies.service';
import { EditContributor, EditMovie } from 'src/app/models/editMovieModel';
import { HttpResponse } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Contributor } from 'src/app/models/contributor';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  editMovie = new EditMovie();
  isLinear = true;
  editContributorList: EditContributor[];


  constructor(
    private contributorsService: ContributorsService,
    private formBuilder: FormBuilder,
    private movieService: MoviesService,
    public dialogRef: MatDialogRef<AddMovieComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Movie
  ) { }

  ngOnInit() {
    this.firstFormGroup = this.formBuilder.group({
      nameCtrl: ['', [Validators.required, Validators.maxLength(128)]],
      titleCtrl: ['', [Validators.required, Validators.maxLength(128)]],
      descCtrl: ['', Validators.maxLength(2048)]
    });

    this.secondFormGroup = this.formBuilder.group({
      genresCtrl: ['', Validators.required]
    });
    this.contributorsService.getAllItems().subscribe((defaultEditContributorList: Contributor[]) => {
      this.editMovie.contributors = defaultEditContributorList.map(({ id, name }) => new EditContributor(id, name));
    }, error => console.log(error));

  }

  private async saveNewMovie() {

    this.editMovie.name = this.firstFormGroup.get('nameCtrl').value;
    this.editMovie.title = this.firstFormGroup.get('titleCtrl').value;
    this.editMovie.description = this.firstFormGroup.get('descCtrl').value;

    return await this.movieService.createMovie(this.editMovie).toPromise().then((response: HttpResponse<any>) => {
      const locationUrl = response.headers.get('Location');
      const splittedParams = locationUrl.split('/');
      this.editMovie.id = splittedParams[splittedParams.length - 1];
    });
  }

  async onSave() {
    await this.saveNewMovie();
    this.dialogRef.close();
  }

}
