import { BaseModel } from './../../models/baseModel';
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-edit-modal',
  templateUrl: 'edit.modal.html',
  styleUrls: ['edit.modal.css'],
})
export class EditModalComponent {
  dialogTitle = 'Add item';

  constructor(
    public dialogRef: MatDialogRef<EditModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: BaseModel) {
    if (data.id) {
      this.dialogTitle = 'Edit Item';
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
