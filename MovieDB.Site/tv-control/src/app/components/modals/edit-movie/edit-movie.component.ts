import { EditContributor } from './../../../models/editMovieModel';
import { EditMovie } from 'src/app/models/editMovieModel';
import { Movie } from 'src/app/models/movie';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MoviesService } from 'src/app/services/movies.service';
import { ContributorsService } from 'src/app/services/contributors.service';

@Component({
  selector: 'app-edit-movie',
  templateUrl: './edit-movie.component.html',
  styleUrls: ['./edit-movie.component.css']
})
export class EditMovieComponent implements OnInit {
  firstFormGroup: FormGroup;
  editMovie: EditMovie = new EditMovie();
  private defaultEditContributorList: EditContributor[];

  constructor(
    private contributorsService: ContributorsService,
    private movieService: MoviesService,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditMovieComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Movie
  ) { }

  onNoClick() {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.firstFormGroup = this.formBuilder.group({
      nameCtrl: ['', Validators.required],
      titleCtrl: ['', Validators.required],
      descCtrl: ['', Validators.required]
    });
    this.getEditMovieModel();
  }

  private async getEditMovieModel() {

    this.movieService.getMovie(this.data.id).subscribe(async movie => {
      this.editMovie.id = movie.id;
      this.editMovie.name = movie.name;
      this.editMovie.title = movie.title;
      this.editMovie.description = movie.description;

      this.editMovie.genres = movie.genres.map(({ id }) => id);

      const defaultEditContributorList = await this.contributorsService.getAllItems().toPromise();
      this.defaultEditContributorList = defaultEditContributorList.map(({ id, name }) => new EditContributor(id, name));
      this.defaultEditContributorList.forEach(editContributor => {
        const { id } = editContributor;
        const activeContributor = movie.contributors.find(({ id: activeContributorId }) => activeContributorId === id);
        editContributor.active = !!activeContributor;
        if (!!activeContributor) {
          editContributor.contributorTypes = activeContributor.contributorTypes.map(({ id: contributorTypeId }) => contributorTypeId);
        }
      });

      this.editMovie.contributors = [...this.defaultEditContributorList];

      this.firstFormGroup.setValue({
        nameCtrl: this.editMovie.name,
        titleCtrl: this.editMovie.title,
        descCtrl: this.editMovie.description,
      });
    });

  }

  private async updateMovie() {
    this.movieService.updateMovie(this.editMovie.id, this.editMovie).toPromise();
  }

  async onSave() {
    await this.updateMovie();
    this.dialogRef.close();
  }
}
