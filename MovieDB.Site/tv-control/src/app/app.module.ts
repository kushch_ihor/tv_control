import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MoviesComponent } from './components/movies/movies.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { ContributorsComponent } from './components/contributors/contributors.component';
import { GenresComponent } from './components/genres/genres.component';
import { ContributorsTypesComponent } from './components/contributors-types/contributors-types.component';
import { environment } from 'src/environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { EditModalComponent } from './components/modals/edit.modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GeneralComponent } from './components/general/general.component';
import { AddMovieComponent } from './components/modals/add-movie/add-movie.component';
import { MatStepperModule } from '@angular/material/stepper';
import { ToolbarComponent } from './components/basics/toolbar/toolbar.component';
import { MatSelectModule } from '@angular/material/select';
import { EditMovieComponent } from './components/modals/edit-movie/edit-movie.component';
import { ContributorSelectionComponent } from './components/basics/contributor-selection/contributor-selection.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MainMovieDataComponent } from './components/basics/main-movie-data/main-movie-data.component';
import { AdditionalMovieDataComponent } from './components/basics/additional-movie-data/additional-movie-data.component';
import { ViroundComponent } from './components/viround/viround.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { UppercaseUnderlineLessPipe } from './pipes/uppercase-underline-less.pipe';
import { LoginComponent } from './components/login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    MoviesComponent,
    ContributorsComponent,
    GenresComponent,
    ContributorsTypesComponent,
    EditModalComponent,
    GeneralComponent,
    AddMovieComponent,
    ToolbarComponent,
    EditMovieComponent,
    ContributorSelectionComponent,
    MainMovieDataComponent,
    AdditionalMovieDataComponent,
    ViroundComponent,
    UppercaseUnderlineLessPipe,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatInputModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatButtonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatStepperModule,
    MatSelectModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  exports: [
    MoviesComponent
  ],
  entryComponents: [EditModalComponent, MoviesComponent, AddMovieComponent, EditMovieComponent],
  providers: [
    { provide: 'BASE_URL', useValue: environment.apiUrl },
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: [] }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
