import { User } from './../models/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private storage: StorageService, private http: HttpClient) {
  }

  async login(userName: string, password: string) {
    const response = await this.http.post('https://viround.com/public/api/authenticate',
      ({ email: userName, password, rememberMe: false })).toPromise() as any;
    const settingsResponse = await this.http.post('https://viround.com/public/api/getUserInformation',
      { data: `{\"_token\":\"${response.token}\"}` }).toPromise() as any;

    const { id, full_user_name } = response.user;
    const user = new User();
    user.id = id;
    user.userName = full_user_name;

    user.homeCity = settingsResponse.userLocation[0].name;
    this.storage.setToken(response.token);
    this.storage.setUser(user);
  }

  get currentUser(): User {
    return this.storage.getUser();
  }

  isLogin(): boolean {
    return !!this.storage.getToken();
  }
}
