import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private tokenFieldName = 'appToken';
  private appUser = 'appUser';
  constructor() { }

  setToken(token: string) {
    localStorage.setItem(this.tokenFieldName, token);
  }

  getToken(): string {
    return localStorage.getItem(this.tokenFieldName);
  }

  setUser(user: User) {
    localStorage.setItem(this.appUser, JSON.stringify(user));
  }

  getUser(): User {
    return JSON.parse(localStorage.getItem(this.appUser)) as User;
  }

  clearStorage() {
    localStorage.clear();
  }
}
