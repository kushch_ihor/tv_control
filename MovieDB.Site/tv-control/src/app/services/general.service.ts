import { BasehttpService } from './basehttp.service';
import { BaseModel } from './../models/baseModel';
import { Injectable } from '@angular/core';
import { GeneralSercviceInterface } from './contracts/generalSercviceInterface';

@Injectable({
  providedIn: 'root'
})
export class GeneralService<T extends BaseModel> implements GeneralSercviceInterface<T> {
  protected readonly endpointUrl: string;

  constructor(private baseService: BasehttpService) {
  }

  getAllItems() {
    return this.baseService.httpGet<T[]>(this.endpointUrl);
  }

  deleteItem(id: string) {
    return this.baseService.httpDelete(this.endpointUrl, id);
  }

  createItem(entity: T) {
    return this.baseService.httpPost(this.endpointUrl, entity);
  }

  updateItem(entity: T) {
    return this.baseService.httpPut(this.endpointUrl + `/${entity.id}`,
      { name: entity.name, title: entity.title, description: entity.description });
  }

}
