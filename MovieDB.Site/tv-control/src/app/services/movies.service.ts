import { EditMovie } from './../models/editMovieModel';
import { Movie } from './../models/movie';
import { BasehttpService } from './basehttp.service';
import { Injectable } from '@angular/core';
import { ID } from '../models/baseModel';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  private endpontUrl = 'movies';
  constructor(private baseService: BasehttpService) { }

  getAllMovies() {
    return this.baseService.httpGet<Movie[]>(this.endpontUrl);
  }

  getMovie(movieId: ID) {
    return this.baseService.httpGet<Movie>(this.endpontUrl + `/${movieId}`);
  }

  deletemovie(id: ID) {
    return this.baseService.httpDelete(this.endpontUrl, id);
  }

  createMovie(entity: EditMovie) {
    return this.baseService.httpPost(this.endpontUrl, {
      id: entity.id,
      name: entity.name,
      title: entity.title,
      description: entity.description,
      genreIds: entity.genres,
      contributors: entity.contributors.filter(item => item.active)
        .map(({ id, contributorTypes }) => ({ id, contributorTypeIds: contributorTypes }))
    });
  }



  updateMovie(movieId: ID, editMovie: EditMovie) {
    return this.baseService.httpPut(this.endpontUrl + `/${movieId}`,
      {
        id: editMovie.id,
        name: editMovie.name,
        title: editMovie.title,
        description: editMovie.description,
        genreIds: editMovie.genres,
        contributors: editMovie.contributors.filter(item => item.active)
          .map(({ id, contributorTypes }) => ({ id, contributorTypeIds: contributorTypes }))
      });
  }
}
