import { Contributor } from './../models/contributor';
import { Injectable } from '@angular/core';
import { GeneralService } from './general.service';

@Injectable({
  providedIn: 'root'
})
export class ContributorsService extends GeneralService<Contributor> {
  protected readonly endpointUrl = 'contributors';
}
