import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class ViroundService {
  private httpOptions: { headers; } = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this.storage.getToken()}`
    })
  };

  constructor(private http: HttpClient, private storage: StorageService) { }

  async getEventsList(id: string): Promise<any> {
    const events = await this.http.post<any>('https://viround.com/public/api/getEvents',
      JSON.stringify({ data: `{"range":0,"FilterVal":"myEvents","user_id":"${id}","results_from":0,"results_num":250}` }),
      this.httpOptions).toPromise();

    return events;
  }

  async getCathegories(): Promise<any> {
    const categories = await this.http.post<any>('https://viround.com/public/api/getCategories',
      {}, this.httpOptions).toPromise();
    return categories;
  }

  async getEventsByCyty(city: string): Promise<any> {
    let filterValue = '{\"lat\":50.4501,\"long\":30.523400000000038,\"name\":\"Kyiv, Ukraine, 02000\"}';
    switch (city) {
      case 'Sumy':
        filterValue = '{\"lat\":50.9077,\"long\":34.79809999999998,\"name\":\"Sumy, Sumy Oblast, Ukraine, 40000\"}';
        break;
      case 'Lviv':
        filterValue = '{\"lat\":49.83968300000001,\"long\":24.029717000000005,\"name\":\"Lviv, Lviv Oblast, Ukraine, 79000\"}';
        break;
      case 'Kharkiv':
        filterValue = '{\"lat\":49.9935,\"long\":36.230383000000074,\"name\":\"Kharkiv, Kharkiv Oblast, Ukraine\"}';
    }

    const events = await this.http.post<any>('https://viround.com/public/api/getEvents',
      JSON.stringify({
        data: `{\"range\":1,\"lat\":50.9077,\"long\":34.7981,\"SearchVal\":${filterValue}
      ,\"FilterVal\":\"Location\",\"_token\":\"${this.storage.getToken()}
      \",\"category\":\"\",\"parentCategory\":\"\",\"results_from\":0,\"results_num\":500}` }),
      this.httpOptions).toPromise();
    return events;
  }
}
