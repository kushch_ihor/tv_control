import { ContributorType } from './../models/contributorType';
import { Injectable } from '@angular/core';
import { GeneralService } from './general.service';

@Injectable({
  providedIn: 'root'
})
export class ContributorsTypesService extends GeneralService<ContributorType> {
  protected readonly endpointUrl = 'contributortypes';
}
