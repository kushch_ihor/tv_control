import { Observable } from 'rxjs';

export interface GeneralSercviceInterface<T> {
    getAllItems(): Observable<T[]>;
    deleteItem(id: string): Observable<object>;
    createItem(entity: T): Observable<object>;
    updateItem(entity: T): Observable<object>;
}
