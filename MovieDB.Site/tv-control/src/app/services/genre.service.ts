import { Genre } from './../models/genre';
import { Injectable } from '@angular/core';
import { GeneralService } from './general.service';

@Injectable({
  providedIn: 'root'
})
export class GenreService extends GeneralService<Genre> {
  protected readonly endpointUrl = 'genres';
}
