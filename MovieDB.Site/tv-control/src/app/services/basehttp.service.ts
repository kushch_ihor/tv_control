import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BasehttpService {
  private baseUrl: string;
  private httpOptions: { headers; observe; } = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    observe: 'response'
  };

  constructor(private httpClient: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;

  }

  public httpGet<T>(endpontUrl: string) {
    return this.httpClient.get<T>(`${this.baseUrl}${endpontUrl}`);
  }

  public httpDelete(endpontUrl: string, id: string) {
    return this.httpClient.delete(`${this.baseUrl}${endpontUrl}/${id}`);
  }

  public httpPost<T>(endpontUrl: string, entity: T) {
    return this.httpClient.post(`${this.baseUrl}${endpontUrl}`, JSON.stringify(entity), this.httpOptions);
  }

  public httpPut<T>(endpontUrl: string, entity: T) {
    return this.httpClient.put(`${this.baseUrl}${endpontUrl}`, JSON.stringify(entity), this.httpOptions);
  }
}
